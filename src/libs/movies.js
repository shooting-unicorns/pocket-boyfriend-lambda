const axios = require('axios');
const { SERVER_API } = require('./constants');
const fetcher = axios.create({
  baseURL: SERVER_API,
  timeout: 5000
});

exports.getMovieRecommendations = async (genre) => {
  const { results } = await fetcher
    .get(`/movies`, { params: { genre } })
    .then(result => result.data);

  const movies = results.map(({ title }) => title);
  console.log('\n ==> TITLES', title);
  return movies;
}

exports.getMovieRecommendationsWithFilter = async (filter) => {
  const params = filter.genre ? {
    genre: filter.genre
  } : {
    cast: filter.celebrity
  }
  const movie = await fetcher
    .get(`/v1/movies`,
      {
        params
      }
    )
    .then(result => result.data);
    return movie;
}

exports.savePreferences = async (userId, payload) => {
  console.log('==> saving', userId)
  const result =  fetcher
    (`/v1/user/${userId}/updatePreferences`, {
      method: 'PUT',
      data: payload
    }).then(res => res.data)

    return result
}

exports.getUserPreferences = async (userId) => {
  return fetcher
  .get(`/v1/user/${userId}/preferences`)
  .then(res => res.data);
}
