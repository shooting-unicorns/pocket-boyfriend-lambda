exports.close = ({ sessionAttributes, fulfillmentState, message }) => {
  return {
    sessionAttributes,
    dialogAction: {
      type: 'Close',
      fulfillmentState,
      message
    }
  }
};

exports.delegate = ({ sessionAttributes, slots }) => {
  return {
    sessionAttributes,
    dialogAction: {
      type: 'Delegate',
      slots,
    }
  }
}

exports.elicitSlot = ({
  sessionAttributes,
  intentProperties,
  slots,
  message,
  slotToElicit
}) => {
  const { intentName } = intentProperties;
  return {
    dialogAction: {
      type: 'ElicitSlot',
      intentName,
      message,
      slots,
      slotToElicit
    }
  }
}

exports.confirm = ({ sessionAttributes, intentProperties, slots, message }) => {
  const { intentName } = intentProperties;
  return {
    sessionAttributes,
    dialogAction: {
      intentName,
      slots,
      message,
      type: "ConfirmIntent"
    }
  }
}
